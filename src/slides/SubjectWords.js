import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import activeVerbs from '~/lib/active-verbs'
import adjectives from '~/lib/adjectives'
import adverbs from '~/lib/adverbs'
import nouns from '~/lib/nouns'
import passiveVerbs from '~/lib/passive-verbs'
import styled from 'styled-components'
import { capitalize } from '~/lib/text'
import { initialize, randomInt, randomItem } from '@joshforisha/utils'
import { randomLetter } from '~/lib/random'

const Phrase = styled.text``

const Title = styled.text`
  font-size: 1.5em;
  text-anchor: middle;
`

export function generate () {
  return new Promise(resolve => {
    const number = randomInt(3, 5)
    const subject = capitalize(randomItem(nouns))
    const letter = randomLetter()

    const matchingActiveVerbs = activeVerbs.filter(v => v.startsWith(letter))
    const matchingPassiveVerbs = passiveVerbs.filter(v => v.startsWith(letter))
    const matchingNouns = nouns.filter(n => n.startsWith(letter))
    const matchingAdjectives = adjectives.filter(a => a.startsWith(letter))

    const phrases = initialize(number, () =>
      randomItem([
        () =>
          `${capitalize(randomItem(matchingActiveVerbs))} ${randomItem(nouns)}`,
        () =>
          `${capitalize(randomItem(matchingAdjectives))} ${randomItem(nouns)}`,
        () => capitalize(randomItem(matchingNouns)),
        () =>
          `${capitalize(randomItem(matchingPassiveVerbs))} ${randomItem(
            adverbs
          )}`
      ])()
    )

    resolve(
      <SubjectWords
        letter={letter.toUpperCase()}
        number={number}
        phrases={phrases}
        subject={subject}
      />
    )
  })
}

export default function SubjectWords ({ letter, number, phrases, subject }) {
  const phraseItems = phrases.map((phrase, i) => (
    <Phrase key={i} x='280' y={250 + i * 50}>
      {i + 1}. {phrase}
    </Phrase>
  ))

  return (
    <SvgSlide>
      <Title x='400' y='100'>
        The {number} {letter}s of {subject}
      </Title>
      {phraseItems}
    </SvgSlide>
  )
}

SubjectWords.propTypes = {
  letter: PropTypes.string,
  number: PropTypes.number,
  phrases: PropTypes.arrayOf(PropTypes.string),
  subject: PropTypes.string
}
