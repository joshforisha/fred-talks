import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import activeVerbs from '~/lib/active-verbs'
import adjectives from '~/lib/adjectives'
import adverbs from '~/lib/adverbs'
import nouns from '~/lib/nouns'
import passiveVerbs from '~/lib/passive-verbs'
import pluralize from 'pluralize'
import styled from 'styled-components'
import { capitalize } from '~/lib/text'
import { chance, initialize, randomInt, randomItem } from '@joshforisha/utils'

const Step = styled.text``

const Title = styled.text`
  font-size: 1.5em;
  text-anchor: middle;
`

export function generate () {
  return new Promise(resolve => {
    const mainNoun = chance(0.5)
      ? pluralize(randomItem(nouns))
      : randomItem(nouns)

    const secondaryNoun = randomItem(nouns)

    const steps = initialize(randomInt(3, 5), () => {
      const active = chance(0.5)
      const verb = randomItem(active ? activeVerbs : passiveVerbs)
      const noun = active
        ? chance(0.5) ? mainNoun : secondaryNoun
        : randomItem(adverbs)
      return `${capitalize(verb)} ${noun}`
    })

    const goal = `${capitalize(randomItem(adjectives))} ${capitalize(
      mainNoun
    )}`

    resolve(<Steps goal={goal} steps={steps} />)
  })
}

export default function Steps ({ goal, steps }) {
  const stepItems = steps.map((text, i) => (
    <Step key={i} x='280' y={250 + i * 50}>
      {i + 1}. {text}
    </Step>
  ))

  return (
    <SvgSlide>
      <Title x='400' y='100'>
        Steps to {goal}
      </Title>
      {stepItems}
    </SvgSlide>
  )
}

Steps.propTypes = {
  goal: PropTypes.string,
  steps: PropTypes.arrayOf(PropTypes.string)
}
