import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import jsonp from '~/lib/jsonp'
import styled from 'styled-components'

const AuthorText = styled.text`
  font-family: cursive;
  text-anchor: end;
`

const QuoteText = styled.p`
  font-family: cursive;
  margin: 180px 60px 0;
  text-align: left;
  width: 700px;
`

export function generate () {
  return jsonp('http://api.forismatic.com/api/1.0/', 'jsonp', {
    format: 'jsonp',
    lang: 'en',
    method: 'getQuote'
  }).then(data => (
    <Quote author={data.quoteAuthor} text={data.quoteText.replace(/ $/, '')} />
  ))
}

export default function Quote ({ author, text }) {
  const authorText =
    author !== ''
      ? (
        <AuthorText x='740' y='400'>
          —{author}
        </AuthorText>
        ) : null

  return (
    <SvgSlide>
      <foreignObject>
        <QuoteText>&ldquo;{text}&rdquo;</QuoteText>
      </foreignObject>
      {authorText}
    </SvgSlide>
  )
}

Quote.propTypes = {
  author: PropTypes.string,
  text: PropTypes.string
}
