import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import adjectives from '~/lib/adjectives'
import adverbs from '~/lib/adverbs'
import nouns from '~/lib/nouns'
import pluralize from 'pluralize'
import styled from 'styled-components'
import { capitalize } from '~/lib/text'
import {
  chance,
  initialize,
  randomItem,
  takeRandomItems
} from '@joshforisha/utils'

const ConsColumn = styled.g`
  fill: ${Color.Red};
`

const ProsColumn = styled.g`
  fill: ${Color.Green};
`

const Subtitle = styled.text`
  font-size: 1.25em;
  text-anchor: start;
`

const Title = styled.text`
  font-size: 1.5em;
  text-anchor: middle;
`

export function generate () {
  return new Promise(resolve => {
    const adjs = takeRandomItems(adjectives, 4)
    const advs = takeRandomItems(adverbs, 4)

    const gen = start => i =>
      chance(0.5)
        ? `${capitalize(advs[start + i])} ${adjs[start + i]}`
        : capitalize(adjs[start + i])

    const pros = initialize(2, gen(0))
    const cons = initialize(2, gen(2))
    const title = capitalize(pluralize(randomItem(nouns)))

    resolve(<ProsAndCons cons={cons} pros={pros} title={title} />)
  })
}

export default function ProsAndCons ({ cons, pros, title }) {
  const proLines = pros.map((pro, i) => (
    <text key={i} x='80' y={300 + i * 50}>
      {pro}
    </text>
  ))

  const conLines = cons.map((con, i) => (
    <text key={i} textAnchor='start' x='480' y={300 + i * 50}>
      {con}
    </text>
  ))

  return (
    <SvgSlide>
      <Title x='400' y='100'>
        {title}
      </Title>
      <ProsColumn>
        <Subtitle x='80' y='240'>
          Pros
        </Subtitle>
        {proLines}
      </ProsColumn>
      <ConsColumn>
        <Subtitle x='480' y='240'>
          Cons
        </Subtitle>
        {conLines}
      </ConsColumn>
    </SvgSlide>
  )
}

ProsAndCons.propTypes = {
  cons: PropTypes.arrayOf(PropTypes.string),
  pros: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string
}
