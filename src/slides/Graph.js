import React from 'react'
import adjectives from '../adjectives'
import nouns from '../nouns'
import pluralize from 'pluralize'
import quantifiers from '../quantifiers'
import { randomChance, randomItem } from '../random'

export default function randomGraph () {
  const type = randomChance(50) ? 'bar' : 'line'

  return (
    <p className='ma0'>
      A {type} graph showing {randomItem(adjectives)} {randomItem(nouns)}{' '}
      {pluralize(randomItem(nouns))} {randomItem(quantifiers)}
    </p>
  )
}
