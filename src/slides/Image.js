import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import adjectives from '~/lib/adjectives'
import jsonp from '~/lib/jsonp'
import nouns from '~/lib/nouns'
import pluralize from 'pluralize'
import styled from 'styled-components'
import { chance, randomItem } from '@joshforisha/utils'

const Img = styled.img`
  height: 100%;
  object-fit: cover;
  width: 100%;
`

const Wrapper = styled.div`
  background-color: ${Color.Silver};
  height: 600px;
  max-width: 100%;
  position: relative;
  text-align: center;
  width: 800px;
`

export function generate () {
  return new Promise(resolve => {
    const adjective = randomItem(adjectives)
    const plural = chance(50)
    const noun = plural ? pluralize(randomItem(nouns)) : randomItem(nouns)

    jsonp(
      'http://api.flickr.com/services/feeds/photos_public.gne',
      'jsoncallback',
      {
        format: 'json',
        tagmode: 'any',
        tags: adjective + ',' + noun
      }
    ).then(res => {
      const src = randomItem(res.items).media.m.replace(/_m/, '_b')
      resolve(<Image alt={`${adjective} ${noun}`} src={src} />)
    })
  })
}

export default function Image ({ alt, src }) {
  return (
    <Wrapper>
      <Img alt={alt} src={src} />
    </Wrapper>
  )
}

Image.propTypes = {
  alt: PropTypes.string,
  src: PropTypes.string
}
