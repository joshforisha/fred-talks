import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import adjectives from '~/lib/adjectives'
import nouns from '~/lib/nouns'
import pluralize from 'pluralize'
import styled from 'styled-components'
import {
  chance,
  initialize,
  randomItem,
  takeRandomItems
} from '@joshforisha/utils'

const LeftCircle = styled.circle`
  background-color: ${p => p.color};
  opacity: 0.5;
`

const RightCircle = styled.circle`
  background-color: ${p => p.color};
  opacity: 0.5;
`

const Text = styled.text`
  text-anchor: middle;
`

const colors = [
  Color.Aqua,
  Color.Blue,
  Color.Fuchsia,
  Color.Green,
  Color.Lime,
  Color.Maroon,
  Color.Navy,
  Color.Olive,
  Color.Orange,
  Color.Purple,
  Color.Red,
  Color.Teal,
  Color.Yellow
]

export function generate () {
  return new Promise(resolve => {
    const selectedAdjectives = takeRandomItems(adjectives, 3)

    const selectedColors = takeRandomItems(colors, 2)

    const selectedNouns = initialize(
      3,
      () => (chance(0.5) ? pluralize(randomItem(nouns)) : randomItem(nouns))
    )

    resolve(
      <VennDiagram
        adjectives={selectedAdjectives}
        colors={selectedColors}
        nouns={selectedNouns}
      />
    )
  })
}

export default function VennDiagram ({ adjectives, colors, nouns }) {
  return (
    <SvgSlide>
      <LeftCircle cx='35%' cy='50%' fill={colors[0]} r='250' />
      <RightCircle cx='65%' cy='50%' fill={colors[1]} r='250' />
      <Text x='19%' y='47%'>
        {adjectives[0]}
      </Text>
      <Text x='19%' y='53%'>
        {nouns[0]}
      </Text>
      <Text x='50%' y='47%'>
        {adjectives[1]}
      </Text>
      <Text x='50%' y='53%'>
        {nouns[1]}
      </Text>
      <Text x='81%' y='47%'>
        {adjectives[2]}
      </Text>
      <Text x='81%' y='53%'>
        {nouns[2]}
      </Text>
    </SvgSlide>
  )
}

VennDiagram.propTypes = {
  adjectives: PropTypes.arrayOf(PropTypes.string),
  colors: PropTypes.arrayOf(PropTypes.string),
  nouns: PropTypes.arrayOf(PropTypes.string)
}
