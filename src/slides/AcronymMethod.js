import PropTypes from 'prop-types'
import React from 'react'
import SvgSlide from '~/components/SvgSlide'
import activeVerbs from '~/lib/active-verbs'
import adverbs from '~/lib/adverbs'
import nouns from '~/lib/nouns'
import passiveVerbs from '~/lib/passive-verbs'
import styled from 'styled-components'
import { chance, randomItem } from '@joshforisha/utils'

const Line = styled.text``

const Title = styled.text`
  font-size: 1.5em;
  text-anchor: middle;
`

export function generate () {
  return new Promise(resolve => {
    const acronym = randomItem(nouns.filter(n => n.length < 7))

    const lines = Array.prototype.map.call(acronym, (letter, index) => {
      const active = chance(0.5)

      const matchingVerbs = (active ? activeVerbs : passiveVerbs).filter(v =>
        v.startsWith(letter)
      )

      const phrase =
        matchingVerbs.length > 0
          ? `${randomItem(matchingVerbs).slice(1)} ${
              active ? randomItem(nouns) : randomItem(adverbs)
            }`
          : ' ???'

      return [<tspan key={index}>{letter.toUpperCase()}</tspan>, phrase]
    })

    resolve(<AcronymMethod acronym={acronym} lines={lines} />)
  })
}

export default function AcronymMethod ({ acronym, lines }) {
  const lineNodes = lines.map((line, i) => (
    <Line key={i} x='280' y={200 + i * 50}>
      {i + 1}. {line}
    </Line>
  ))

  return (
    <SvgSlide>
      <Title x='400' y='100'>
        The {acronym.toUpperCase()} Method
      </Title>
      {lineNodes}
    </SvgSlide>
  )
}

AcronymMethod.propTypes = {
  acronym: PropTypes.string,
  lines: PropTypes.arrayOf(PropTypes.node)
}
