export const generators = [
  require('./AcronymMethod').generate,
  require('./Image').generate,
  require('./ProsAndCons').generate,
  require('./Quote').generate,
  require('./Steps').generate,
  require('./SubjectWords').generate,
  require('./VennDiagram').generate
]
