import { entries } from '@joshforisha/utils'

function generateCallbackName () {
  return `jsonp_callback_${Date.now()}_${Math.round(99999 * Math.random())}`
}

export default function jsonp (url, callbackName, args = {}) {
  return new Promise(resolve => {
    const script = document.createElement('script')
    script.setAttribute('type', 'application/javascript')

    const callbackAlias = generateCallbackName()

    const argsString = entries(args)
      .concat([[callbackName, callbackAlias]])
      .map(pair => pair.join('='))
      .join('&')

    window[callbackAlias] = data => {
      delete window[callbackAlias]
      document.body.removeChild(script)
      resolve(data)
    }

    script.setAttribute('src', `${url}?${argsString}`)
    document.body.appendChild(script)
  })
}
