export function without (object, ...keys) {
  return Object.keys(object)
    .filter(k => keys.indexOf(k) === -1)
    .reduce((o, k) => Object.assign(o, { [k]: object[k] }), {})
}
