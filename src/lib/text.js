export function capitalize (string) {
  if (string.length === 0) return string
  return `${string[0].toUpperCase()}${string.substr(1)}`
}
