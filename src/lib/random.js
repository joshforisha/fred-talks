import { shuffle } from './array'

export function randomChance (number) {
  return Math.random() * 100 < number
}

export function randomItem (items) {
  if (items.length === 0) return null
  return items[Math.floor(Math.random() * items.length)]
}

export function randomItems (items, count) {
  const shuffledItems = shuffle(items)

  const choices = []
  while (choices.length < count) choices.push(shuffledItems[choices.length])

  return choices
}

export function randomLetter () {
  return randomItem([
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'y'
  ])
}

export function randomNumber (min, max) {
  const span = max - min
  return Math.floor(Math.random() * span) + min
}
