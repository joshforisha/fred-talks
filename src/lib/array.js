export function generateArray (count, genFn) {
  const array = []
  for (let index = 0; index < count; index++) array.push(genFn(index))
  return array
}

export function shuffle (array) {
  const copy = array.slice(0)
  const newArray = []
  while (copy.length > 0) { newArray.push(copy.splice(Math.floor(Math.random() * copy.length), 1)[0]) }
  return newArray
}
