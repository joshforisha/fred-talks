import Color from 'lib/Color'

// Theme: { dark, light, main, contrastText }

const Theme = {
  Default: {
    background: Color.White,
    foreground: Color.Charcoal
  }
  // AAA Themes
  // AA Themes
  // AA18 Themes
}

export default Theme

export function inverse (theme) {
  return {
    background: theme.foreground,
    foreground: theme.background
  }
}
