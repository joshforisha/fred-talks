const Color = {
  Aqua: '#7FDBFF',
  Black: '#111111',
  Blue: '#0074D9',
  Charcoal: '#444444',
  Fuchsia: '#F012BE',
  Gray: '#AAAAAA',
  Green: '#2ECC40',
  Lime: '#01FF70',
  Maroon: '#85144B',
  Navy: '#001f3f',
  Olive: '#3D9970',
  Orange: '#FF851B',
  Purple: '#B10DC9',
  Red: '#FF4136',
  Silver: '#DDDDDD',
  Teal: '#39CCCC',
  White: '#FFFFFF',
  Yellow: '#FFDC00'
}

export default Color
