export const Method = {
  Delete: 'DELETE',
  Get: 'GET',
  Post: 'POST',
  Put: 'PUT'
}

export function get (url) {
  return request(Method.Get, url, null)
}

export function request (method, url, data) {
  return new Promise((resolve, reject) => {
    const xhr = new window.XMLHttpRequest()

    xhr.addEventListener('load', () => {
      resolve(JSON.parse(xhr.responseText))
    })

    xhr.open(method, url, true)
    xhr.send(data ? JSON.stringify(data) : null)
  })
}
