export default [
  'across countries',
  'across languages',
  'across timezones',
  'along meridians',
  'over days',
  'over decades',
  'over months',
  'over milliseconds',
  'over seconds',
  'over weeks',
  'over years'
]
