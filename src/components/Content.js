import styled from 'styled-components'

export default styled.div`
  margin: 0 auto;
  max-width: calc(800px + 2rem);
  padding: 1rem;
`
