import Icon from '~/components/Icon'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Wrapper = styled(Icon)`
  animation-duration: 2000ms;
  animation-iteration-count: infinite;
  animation-name: spin;
  animation-timing-function: linear;
  font-size: 0.9em;

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }

    to {
      transform: rotate(360deg);
    }
  }
`

export default function LoadingIcon (props) {
  return <Wrapper className={props.className} name='loader' />
}

LoadingIcon.propTypes = {
  className: PropTypes.string
}
