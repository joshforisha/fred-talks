import Button from '~/components/Button'
import Color from '~/lib/Color'
import Content from '~/components/Content'
import Icon from '~/components/Icon'
import LoadingIcon from '~/components/LoadingIcon'
import React from 'react'
import SpeakerPage from '~/components/SpeakerPage'
import styled from 'styled-components'
import topics from '~/lib/topics'
import { generators } from '~/slides'
import { randomItem, setItem } from '@joshforisha/utils'

const App = styled.div`
  font-family: system-ui, 'Segoe UI', 'Helvetica Neue', sans-serif;
  line-height: 1.5;
`

const Header = styled.header`
  border-bottom: 1px solid ${Color.Silver};
  border-top: 4px solid ${Color.Fuchsia};
  line-height: 1;
`

const RefreshButton = styled(Button)`
  float: right;
  padding-top: 0.25rem;
`

const TitleSpan = styled.span`
  color: ${Color.Fuchsia};
  font-size: 2rem;
  font-weight: 800;
`

export default class Application extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      busy: true,
      currentSlide: 1,
      slides: [],
      subject: null,
      topic: null
    }

    this.changeSlide = this.changeSlide.bind(this)
    this.generatePrompt = this.generatePrompt.bind(this)
    this.setSlide = this.setSlide.bind(this)
  }

  componentDidMount () {
    this.generatePrompt()
  }

  changeSlide (index) {
    this.setState(() => ({ currentSlide: index }))
  }

  generatePrompt () {
    const subjectPromise = window
      .fetch(
        'https://en.wikipedia.org/w/api.php?action=query&format=json&list=random&origin=*&rnnamespace=0&rnlimit=1'
      )
      .catch(err => window.console.error(err))
      .then(res => res.json())
      .then(data => {
        this.setState(() => ({
          subject: data.query.random[0].title
        }))
      })

    const slide1Promise = randomItem(generators)().then(this.setSlide(0))
    const slide2Promise = randomItem(generators)().then(this.setSlide(1))
    const slide3Promise = randomItem(generators)().then(this.setSlide(2))

    Promise.all([
      subjectPromise,
      slide1Promise,
      slide2Promise,
      slide3Promise
    ]).then(() => {
      this.setState({ busy: false })
    })

    this.setState(() => ({
      busy: true,
      currentSlide: 1,
      slides: [
        <LoadingIcon key='1' />,
        <LoadingIcon key='2' />,
        <LoadingIcon key='3' />
      ],
      subject: null,
      topic: randomItem(topics)
    }))
  }

  setSlide (index) {
    return slide => {
      this.setState(state => ({
        slides: setItem(state.slides, index, slide)
      }))
    }
  }

  render () {
    return (
      <App>
        <Header>
          <Content>
            <TitleSpan>FRED</TitleSpan>
            <RefreshButton
              disabled={this.state.busy}
              onClick={this.generatePrompt}
            >
              <Icon name='refresh-cw' />
            </RefreshButton>
          </Content>
        </Header>
        <Content>
          <SpeakerPage
            currentSlide={this.state.currentSlide}
            onChangeSlide={this.changeSlide}
            slides={this.state.slides}
            subject={this.state.subject}
            topic={this.state.topic}
          />
        </Content>
      </App>
    )
  }
}
