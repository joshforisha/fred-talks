import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Btn = styled.button`
  background-color: ${p => (p.disabled ? Color.Gray : Color.Fuchsia)};
  border-width: 0;
  color: ${Color.White};
  cursor: ${p => (p.disabled ? 'not-allowed' : 'pointer')};
  font-size: 1em;
  font-variant: small-caps;
  opacity: ${p => (p.disabled ? 0.5 : 1)};
  outline: none;
  transition: background-color 100ms ease-out, color 100ms ease-out;

  :hover {
    background-color: ${p => (p.disabled ? null : Color.Purple)};
  }
`

export default function Button (props) {
  return (
    <Btn
      className={props.className}
      disabled={props.disabled}
      onClick={props.onClick}
    >
      {props.children}
    </Btn>
  )
}

Button.defaultTypes = {
  disabled: false
}

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func
}
