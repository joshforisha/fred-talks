import PropTypes from 'prop-types'
import React from 'react'
import feather from 'feather-icons'
import styled from 'styled-components'

const Wrapper = styled.span`
  display: inline-block;
  height: 1.5em;
  line-height: 1;
  width: 1.5em;

  > svg {
    height: 1.5em;
    padding: 0.25rem;
    width: 1.5em;
  }
`

export default function Icon (props) {
  if (!(props.name in feather.icons)) return null

  return (
    <Wrapper
      className={props.className}
      inline={props.inline}
      dangerouslySetInnerHTML={{ __html: feather.icons[props.name].toSvg() }}
    />
  )
}

Icon.defaultProps = {
  inline: false
}

Icon.propTypes = {
  className: PropTypes.string,
  inline: PropTypes.bool,
  name: PropTypes.string.isRequired
}
