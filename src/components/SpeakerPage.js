import ExternalLink from '~/components/ExternalLink'
import LoadingIcon from '~/components/LoadingIcon'
import PropTypes from 'prop-types'
import React from 'react'
import Slab from '~/components/Slab'
import Switcher from '~/components/Switcher'
import styled from 'styled-components'

const TopicSlab = styled(Slab)`
  margin-right: 1rem;
`

export default function SpeakerPage (props) {
  const subject =
    props.subject !== null
      ? (
        <ExternalLink href={`https://en.m.wikipedia.org/wiki/${props.subject}`}>
          {props.subject}
        </ExternalLink>
        ) : (
          <LoadingIcon />
        )

  const slideOptions = ['Slide 1', 'Slide 2', 'Slide 3']
  const topic = props.topic !== null ? props.topic : <LoadingIcon />

  const currentSlide = props.slides[props.currentSlide - 1]

  return (
    <div>
      <div>
        <TopicSlab label='Topic'>{topic}</TopicSlab>
        <Slab label='Subject'>{subject}</Slab>
      </div>
      <Switcher
        onChange={val => props.onChangeSlide(slideOptions.indexOf(val) + 1)}
        options={slideOptions}
        value={slideOptions[props.currentSlide - 1]}
      />
      {currentSlide}
    </div>
  )
}

SpeakerPage.propTypes = {
  currentSlide: PropTypes.number,
  onChangeSlide: PropTypes.func,
  slides: PropTypes.arrayOf(PropTypes.node),
  subject: PropTypes.string,
  topic: PropTypes.string
}
