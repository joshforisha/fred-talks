import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Link = styled.a`
  color: ${Color.Fuchsia};
  cursor: pointer;
  text-decoration: underline;
  transition: color 100ms ease-out;

  :hover {
    color: ${Color.Purple};
  }
`

export default function ExternalLink (props) {
  return (
    <Link className={props.className} href={props.href} rel='noreferrer' target='_blank'>
      {props.children}
    </Link>
  )
}

ExternalLink.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  href: PropTypes.string
}
