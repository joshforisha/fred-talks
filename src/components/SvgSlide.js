import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Svg = styled.svg`
  background-color: ${Color.Silver};
  font-size: 2rem;
`

export default function SvgSlide (props) {
  return (
    <Svg preserveAspectRatio='xMinYMin' viewBox='0 0 800 600'>
      {props.children}
    </Svg>
  )
}

SvgSlide.propTypes = {
  children: PropTypes.node
}
