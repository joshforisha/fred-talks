import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Label = styled.dd`
  color: ${Color.Gray};
  font-weight: 700;
  line-height: 1;
  margin-left: 0;
  text-transform: uppercase;
`

const Value = styled.dd`
  font-size: 2rem;
  margin-left: 0;
`

const Wrapper = styled.dl`
  display: inline-block;
  vertical-align: top;
`

export default function Slab (props) {
  return (
    <Wrapper className={props.className}>
      <Label>{props.label}</Label>
      <Value>{props.children}</Value>
    </Wrapper>
  )
}

Slab.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  label: PropTypes.string
}
