import Color from '~/lib/Color'
import PropTypes from 'prop-types'
import React from 'react'
import styled from 'styled-components'

const Option = styled.span`
  border-bottom: ${p => (p.selected ? '4px' : '2px')} solid
    ${p => (p.selected ? Color.Purple : Color.Fuchsia)};
  color: ${p => (p.selected ? Color.Purple : Color.Fuchsia)};
  cursor: ${p => (p.selected ? 'normal' : 'pointer')};
  display: inline-block;
  font-size: 1.25rem;
  font-variant: small-caps;
  line-height: 1.25;
  margin-right: 1rem;
  text-align: center;
  transition: border-color 100ms ease-out, color 100ms ease-out;

  :hover {
    border-color: ${p => (p.selected ? null : Color.Purple)};
    color: ${p => (p.selected ? null : Color.Purple)};
  }
`

const Wrapper = styled.div`
  margin: 1rem 0;
`

export default function Switcher (props) {
  const selectedOption =
    props.options.indexOf(props.value) > -1 ? props.value : props.options[0]

  const options = props.options.map(option => {
    const selected = option === selectedOption

    return (
      <Option
        key={option}
        onClick={() => (selected ? null : props.onChange(option))}
        selected={selected}
      >
        {option}
      </Option>
    )
  })

  return <Wrapper>{options}</Wrapper>
}

Switcher.propTypes = {
  onClick: PropTypes.func,
  options: PropTypes.array,
  value: PropTypes.any
}
